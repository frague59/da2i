Projet DA2I 2016
================

Le but de ce projet est de créer un répertoire de personnes :

* Civilité / Nom / prénom
* Numéros de téléphone
* Adresse postale
* Adresse électronique

L'administration de ces données est à effectuer à traver l'interface d'adminisration proposée par django,
pour des utilisateurs identifiés.

La consultation des données peut être effectuée par tout-un-chacun, la recherche s'effectue par nom / prénom,
l'application propose également un annuaire inversé.

À faire
-------

* Compléter le modèle de données
* Compléter l'administration
* Faire les formulaires de recherche
* Faire la / les vues des utilisateurs
* Mettre le tout en production.

Batteries included
------------------

* Exemples de modèles
* Exemples d'administration
* Gabarit 'de base' + statics kivonbien

Installation du projet :
------------------------

.. code:: sh

    $ git clone https://gitlab.com/frague59/da2i.git
    $ cd da2i/
    $ mkdir virtualenv 
    $ virtualenv -p /usr/bin/python3 ./virtualenv
    $ . ./vurtualenv/bin/activate
    $ pip install pip --upgrade
    $ pip install --requirement ./src/requirements/requirements.txt --constraint ./src/requirements/constraints.txt
    $ cd src/directory
    $ ./manage.py migrate 
    $ ./manage.py createsuperuser
    $ ./manage.py runserver


Et visiter le `site de dév <http://localhost:8000/>`_ !


**Bon courage !**
