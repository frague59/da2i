"""
Development settings for :mod:`directory` project
"""
from directory.settings import *
DEBUG = True

if DEBUG:
    INSTALLED_APPS.append('debug_toolbar')

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),
        },

        'persons': {
            'handlers': ['console'],
            'level': 'DEBUG',
        }
    },
}
