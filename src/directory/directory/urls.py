"""directory URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from persons import models

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^persons/$', ListView.as_view(model=models.Person), name='person_list'),
    url(r'^persons/(?P<pk>\d+)/$', DetailView.as_view(model=models.Person))
]
