"""
Admin for the :mod:`persons` application
"""
import logging
from django.contrib import admin
from persons import models

logger = logging.getLogger('persons.admin')


class AdministrableAdmin(admin.ModelAdmin):
    """
    Provides admin features for :class:`persons.models.Administrable` overrider models
    """
    def save_model(self, request, obj, form, change):
        """
        Binds user to user_create / user_update fields on the object
        """
        if change:
            obj.user_update = request.user
        else:
            obj.user_create = request.user
        super(AdministrableAdmin, self).save_model(request=request, obj=obj, form=form, change=change )


class PersonAdmin(AdministrableAdmin):
    """
    Admin for :class:`persons.models.Person`
    """
    list_display = ('civility', 'last_name', 'first_name')


admin.site.register(models.Civility, AdministrableAdmin)
admin.site.register(models.PhoneNumber, AdministrableAdmin)
admin.site.register(models.EmailAddress, AdministrableAdmin)
admin.site.register(models.PostalAddress, AdministrableAdmin)
admin.site.register(models.Person, PersonAdmin)

