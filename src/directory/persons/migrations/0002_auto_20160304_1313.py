# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-03-04 13:13
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('persons', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='person',
            options={'ordering': ('last_name', 'first_name'), 'verbose_name': 'Personne', 'verbose_name_plural': 'Personnes'},
        ),
    ]
