"""
Models for :mod:`persons` application

from django.db import models
"""
import logging
from django.db import models

#: logger for :mod:`persons.models`
logger = logging.getLogger('persons.models')

PHONE_DEVICES_CHOICES = (('MOBILE', 'Mobile'),
                         ('FIXED', 'Filaire'),
                         ('FAX', 'Fax'),
                         )


class Administrable(models.Model):
    """
    Abstract model that provides administrations features for models
    """
    #: Date of creation.
    date_create = models.DateTimeField(auto_now_add=True, editable=False)

    #: Date of update.
    date_update = models.DateTimeField(auto_now=True, null=True, blank=True, editable=False)

    #: User that created the instance. TODO: Bind field to request user on admin
    user_create = models.ForeignKey('auth.User', editable=False, related_name='%(app_label)s_%(class)s_creator')

    #: User that updated the instance. TODO: Bind field to request user on admin
    user_update = models.ForeignKey('auth.User', null=True, blank=True, editable=False, related_name='%(app_label)s_%(class)s_updater')

    class Meta:
        abstract = True


class Person(Administrable):
    """
    A :class:`persons.models.Person` represents a people
    """
    #: civility of a person
    civility = models.ForeignKey('persons.Civility')

    #: Last name of the person
    last_name = models.CharField(max_length=255)
    
    #: First name of the person
    first_name = models.CharField(max_length=255)
    
    #: Birth date (Optional)
    date_birth = models.DateField(null=True, blank=True)

    def __str__(self):
        return '%s %s %s' % (self.civility.label_short, self.last_name, self.first_name)

    class Meta:
        ordering = ('last_name', 'first_name',)
        verbose_name = 'Personne'
        verbose_name_plural = 'Personnes'


class Civility(Administrable):
    """
    Provides a civility
    """

    #: normal (long) label
    label = models.CharField(max_length=50)

    #: shortened label
    label_short = models.CharField(max_length=50)

    class Meta:
        ordering = ('label',)

    def __str__(self):
        return self.label


class PhoneNumber(Administrable):
    """
    Phone numbers for peoples
    """
    #: Phone number
    number = models.CharField(max_length=10,)

    #: :class:`persons.models.Person` linked with this number
    person = models.ForeignKey('persons.Person', default=1)

    #: private
    private = models.BooleanField(default=True, help_text='Ce numéro de téléphone est privé.')

    #: Device type linked to this phone number
    device_type = models.CharField(max_length=50, choices=PHONE_DEVICES_CHOICES, default='FIXED')
    
    class Meta:
        ordering = ('number',)
        verbose_name = 'Telephone'
        verbose_name_plural = 'Telephones'

    def __str__(self):
        return self.number

class EmailAddress(Administrable):
    """
    Email address for peoples
    """
    #: Email address
    email = models.CharField(max_length=10)
    
    #: :class:`persons.models.Person` linked with this email
    person = models.ForeignKey('persons.Person', default=1)
    
    #: private
    private = models.BooleanField(default=True, help_text='Cet email est privé.')
    
    class Meta:
        ordering = ('email',)
        verbose_name = 'Email'
        verbose_name_plural = 'Emails'

    def __str__(self):
        return self.email

class PostalAddress(Administrable):
    """
    Postal address for peoples
    """
    #: Postal code
    postal_code = models.CharField(max_length=5, default=59000)
    
    #: Postal number
    postal_number = models.CharField(max_length=3, default=1)
    
    #: Postal street
    postal_street = models.CharField(max_length=100, default='rue de Lille')
    
    #: :class:`persons.models.Person` linked with this postal address
    person = models.ForeignKey('persons.Person', default=1)
    
    #: private
    private = models.BooleanField(default=True, help_text='Cette adresse est privée.')
    
    class Meta:
        ordering = ('postal_code',)
        verbose_name = 'Adresse postale'
        verbose_name_plural = 'Adresses postales'

    def __str__(self):
        return self.postal_code
