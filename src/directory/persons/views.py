from django.shortcuts import render

from persons import models
from django.views import (ListView, DetailView, FormView)

# Create your views here.
class PersonListView(ListView) :
    """
    Vue associée à une personne
    """
    model = models.Person
